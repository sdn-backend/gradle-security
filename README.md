## Gradle + Spring Security

## 목차
- [인증](#인증)
- [권한](#권한)

## 인증
#### 1. 프로젝트 세팅
1. gradle 명령어로 프로젝트 생성
```shell
해당 루트 파일 생성 후 안에다가 gradle 명령어 실행
gradle init
basic
groovy
...

idea . >> 인텔리제이 터미널 단축키
```

2. `setting.gradle` `build.gradle` 서브 모듈 세팅 및 공통 모듈 세팅

#### 2. spring-security 사용
> 기본 설정은 모든 페이지 로그인 후 입장 가능해짐

1. `application.yml`
> 유저 한명만 설정 가능
```shell
spring:
  security:
    user:
      name: user1
      password: 1111
      roles: USER
```
2. `controller` 설정
```shell
@PreAuthorize("hasAnyAuthority('ROLE_USER')")
@RequestMapping("/user")
public SecurityMessage user() {
    return SecurityMessage.builder()
            .auth(SecurityContextHolder.getContext().getAuthentication())
            .message("User 정보")
            .build();
}
@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
@RequestMapping("/admin")
public SecurityMessage admin() {
    return SecurityMessage.builder()
            .auth(SecurityContextHolder.getContext().getAuthentication())
            .message("Admin 정보")
            .build();
}
```
- 하지만 user 로그인으로 하면 admin 페이지도 볼수 있는 권한이 있다(악의적인 사용 가능)
- 이를 위해 ⭐ `config` 파일을 통해 설정한다 ! 

3. `SecurityConfig` 생성
- 유저 여러명 생성 가능
- 패스워드 인코더 필수
```java
@Bean
PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
}
```
- 기본 홈페이지 로그인 없이 입장 가능하기 위해선 아래와 같이 설정
```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests((requests) ->
            requests.antMatchers("/").permitAll()
    .anyRequest().authenticated());
    http.formLogin();
    http.httpBasic();
}
```

#### 3. 로그인 로직
1. SecurityConfig
    1. configure(HttpSecurity http)
        - 해당 로그인 페이지 접근 가능하게 개발
    2. configure(WebSecurity web)
        - 스프링 시큐리티 css 적용 문제 해결 
    3. configure(AuthenticationManagerBuilder auth) 
        - 기본 user, admin 정보 생성
        - 하지만, 로그인 안되는 문제 발생 >> ✅ csrf 필터가 먼저 적용되기 때문
            - 해당 `login.html` 의 `th:action="@{/login}"` 으로 수정
            - ✅ `th:action` 만으로 자동으로 csrf 토큰 생성 해준다. 
    4. 관리자 > 유저 페이지 접근 권한 설정
```java
@Bean
RoleHierarchy roleHierarchy(){
    RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
    roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_USER");
    return roleHierarchy; 
}
```

#### 4. Authentication
1. Student 통행증
    1. Student.java 
    2. StudentAuthenticationToken.java ✅통행증
    3. StudentManager.java ✅ 통행증 발급
    4. SecurityConfig.java ✅ 통행증 등록
2. Teacher 통행증
    ... 동일
   
3. 권한이 없을 경우

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests(request->
            request.antMatchers("/").permitAll()
            .anyRequest().authenticated()
        )
    .formLogin(
        login->login.loginPage("/login")
        .permitAll()
        .defaultSuccessUrl("/", false)
        .failureUrl("/login-error")
    )
    .logout(logout->logout.logoutSuccessUrl("/"))
    .exceptionHandling(e -> e.accessDeniedPage("/access-denied")) ✅
        ;
        }
```

4. 결과
![](img/teacher.jpg)

5. 커스텀 필터 
- 로그인 타입 선택

```html
<div class="type-select">
    <input type="radio" name="type" value="student" checked> 학생
    <input type="radio" name="type" value="teacher"> 선생님
</div>
```

- CustomLoginFilter.java

```java
@Override
public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    String username = obtainUsername(request);
    username = (username != null) ? username : "";
    username = username.trim();
    String password = obtainPassword(request);
    password = (password != null) ? password : "";

    String type = request.getParameter("type");
    if(type == null || type.equals("teacher")){ ✅ 타입별 로직
        //student
        StudentAuthenticationToken token = StudentAuthenticationToken.builder()
                .credentials(username).build();

        return this.getAuthenticationManager().authenticate(token);
    }else{
        //teacher
        TeacherAuthenticationToken token = TeacherAuthenticationToken.builder()
                .credentials(username)
                .build();
        return this.getAuthenticationManager().authenticate(token);
    }
}
```

##### ⭐ 기존 로그인 `.formLogin` 이랑 같이 사용해도 문제 없다!


#### 5. BasicAuthenticationFilter
- GET 테스트

```java
@DisplayName("3. 인증 성공2")
@Test
void test_3() {
    TestRestTemplate testClient = new TestRestTemplate("user1", "1111");
    String resp = testClient.getForObject(greetingUrl(), String.class);
    assertEquals("hello",  resp);
}
```

- POST 테스트

```java
@DisplayName("4. post 인증")
@Test
void test_4() {
    TestRestTemplate testClient = new TestRestTemplate("user1", "1111");
    ResponseEntity<String> resp = testClient.postForEntity(greetingUrl(), "hyunho", String.class);
    assertEquals("hello hyunho",  resp.getBody());
}
```
- ❌ 테스트 문제 발생
    - `csrf` 작동하기 때문
    - SecurityConfig.java >> `http.csrf().disable()` 설정 !
    - 하지만, ✅ 보안 취약점 발생!!
    
#### 6. ✅ `UserDetailsService`와 `UserDetails`
...

#### 7. 로그인 정보 유지(세션)
- `SecurityContextPersistenceFilter`
- `RememberMeAuthenticationFilter` (세션이 만료되면 동작)
- `AnonymousAuthenticationFilter` (로그인하지 않는 사용자 혹은 검증되지 않은 사용자에게 발급)


## 권한
...
